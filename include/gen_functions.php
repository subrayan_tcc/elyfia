<?php
 
 function login() {
  if(!isset($_SESSION['admin']) || $_SESSION['admin'] === NULL) {
     header('location: login.php') ;
     exit;      
  }
 }
 
 function sanit_data($data) {
   return(addslashes(htmlentities(htmlspecialchars($data))));
 }

 function clean($data) {
   $clean = addslashes(htmlentities(htmlspecialchars($data)));
   return "'".$clean."'";
 }

 function menu_is_active_by_file($filename) {	
	if(is_array($filename)) {
		$good = FALSE;
		foreach($filename as $f) {
			if(strpos($_SERVER['PHP_SELF'], $f)) {
				$good = TRUE;
			}
		}
		
		return ($good) ? 'fa-circle' : 'fa-circle-o';
	}
	return (strpos($_SERVER['PHP_SELF'], $filename) !== FALSE) ? 'fa-circle' : 'fa-circle-o';
}


function db_insert_genric($insert_array,$table){
 foreach($insert_array as $key => $val) {

       $key_val[] = $key;
       $val_val[] = clean($val);

  }
     $key = implode(',',$key_val);
     $val = implode(',',$val_val);
     
      return ("insert into $table ($key) values ($val)");
}

function get_row($id,$conn,$table){

    $sql = "select * FROM $table WHERE id = '$id' ";
    $query =  mysqli_query($conn,$sql) or die(mysqli_error($conn));
    $row = mysqli_fetch_array($query);
    return $row;
}
 
?>

