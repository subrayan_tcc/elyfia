<?php
   require_once('config/config.php');    
   require_once('include/gen_functions.php');
   
   require_once('include/header.php');
    
   ?>
<script src="js/datepicker.js"></script>
<script src="js/script.js"></script>
<html>
	<!-- Header -->
	<header>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="" href="#"><img src="img/logo login.png"></a>	    
	    </div>
		<a href="login.php" id="btn" class="btn btn-info">Sign in</a>
	  </div>
	</nav>
	</header>

	<?php
		if(isset($_SESSION['insert'])){
		      echo "<div class='text-center alert alert-success'><strong>Success! </strong> ". $_SESSION['insert']." </div>";
		      unset($_SESSION['insert']);
		   }
	?>
	<!-- Header -->
<body onload="signupform.reset();">
<!-- Content Left Side -->
<div class="container">
	<div class="head">
		<h1 class="head">Create a Elyfia Account</h1>
	</div>
	<div class="content_left">
		<div class="col-md-6">
			<h4 class="sub_head">Lorem Ipsum is simply dummy text.</h4>
			<p class="para">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			<div class="icon">				
				<img src="img/fitbit.png" class="all_icon"><img src="img/misfit.png" class="all_icon">
				<img src="img/runkeeper.png" class="all_icon"><img src="img/fatsecret.png" class="all_icon">
				<img src="img/23and me.png" class="all_icon"><img src="img/withings.png" class="all_icon">
				<img src="img/strava.png" class="all_icon"><img src="img/jawboneup.png" class="all_icon">
				<img src="img/microsoftband.png" class="all_icon"><img src="img/lumo.png" class="all_icon">
				<img src="img/map mywalk.png" class="all_icon"><img src="img/mapmyfitness.png" class="all_icon">
				<img src="img/daily mile.png" class="all_icon"><img src="img/move app.png" class="all_icon">
				<img src="img/life fitness.png" class="all_icon"><img src="img/google fit.png" class="all_icon">
				<img src="img/sonylife log.png" class="all_icon"><img src="img/predict bgl.png" class="all_icon">
			</div>
			<h3 class="sub_head">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
			<p class="para">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			<div class="col-md-6">				
				<img src="img/ima.png" class="device_signup">
				<img src="img/B-R.png" class="device_signup">
			</div>			
			<div class="col-md-6">
				<img src="img/obj_lifecare.png" class="device_signup">
				<img src="img/googlefit.png" class="device_signup">
			</div>
			<div class="col-md-6">				
				<img src="img/unnamed.png" class="device_signup">
				<img src="img/band.png" class="device_signup">
			</div>		
			<div class="col-md-6">
				<img src="img/slide.png" class="device_signup">
				<img src="img/456.png" class="device_signup">		
			</div>
		        
		</div>	
	</div>
<!-- Content Left Side -->			

	<!-- Signup Box -->
         <div id="signupbox" class="col-md-6">
            <div class="panel panel-info">
               <div class="panel-heading">
                  <div class="panel-title">Sign Up</div>
               </div>
               <div class="panel-body" >
                  <form id="signupform" class="form-horizontal" action="patient_insert.php" enctype="multipart/form-data" method="post">
			<div class="form-group">
                        <label for="firstname" class="col-md-3 control-label">First Name</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="firstname" placeholder="First Name" required>
                        </div>
                        </div>
			<div class="form-group">
                        <label for="lastname" class="col-md-3 control-label">Last Name</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="lastname" placeholder="Last Name" required>
                        </div>
                        </div>
			<div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                           <input type="text" id='email' class="form-control" name="email" placeholder="Email Address" required>
                        </div>
			<center><span id="email_error" style="color:red;"> </span></center>
                        </div>
			<div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                           <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                        </div>

                        </div>
			<div class="form-group">
                        <label for="confirm_password"" class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-9">
                           <input type="password"" class="form-control" id="confirm_password" placeholder="Confirm Password" required>
                        </div>
			<span id="pass_error" style="color:red"> </span>
                        </div>
			<div class="form-group">
                        <label for="date_of_birth" class="col-md-3 control-label">Date Of Birth</label>
                        <div class="col-md-9">
				<div class='input-group date' id='datetimepicker_dob'>
				  <div class="input-group-addon">
				   <span class="glyphicon glyphicon-calendar"></span>
				  </div>
                           	  <input type="text" id="date_of_birth" class="form-control" required name="date_of_birth" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
				</div>
                        </div>
                        </div>
			<div class="form-group">
                        <label for="device" class="col-md-3 control-label">Device</label>
                        <div class="col-md-9">
                        	<select name="device" class="form-control" required>
				<option value=""> Select A Device  </option>							
				<option value="Dailymile"> Dailymile </option>							
				<option value="Fatsecret"> Fatsecret </option>							
				<option value="Fitbit"> Fitbit </option>							
				<option value="GoogleFit"> Google Fit </option>							
				<option value="Jawbone"> Jawbone </option>							
				<option value="Lifefitness"> Lifefitness </option>							
				<option value="MapMyFitness"> MapMyFitness </option>							
				<option value="MapMyRun"> MapMyRun </option>							
				<option value="MapMyWalk"> MapMyWalk </option>							
				<option value="MicrosoftHealth"> Microsoft Health </option>							
				<option value="Misfit"> Misfit </option>							
				<option value="Moves"> Moves </option>							
				<option value="PredicctBGL"> PredicctBGL </option>							
				<option value="Runkeeper"> Runkeeper </option>							
				<option value="Strava"> Strava </option>							
				<option value="SonyLifelog"> Sony Lifelog </option>							
				<option value="Withings"> Withings </option>							
				<option value="23andMe"> 23andMe </option>
				</select>
			</div>
                        </div>
			<div class="form-group">
                        <label for="home_phone" class="col-md-3 control-label">Home Phone</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="home_phone" placeholder="Home Phone">
                        </div>
                        </div>
			<div class="form-group">
                        <label for="office_phone" class="col-md-3 control-label">Office Phone</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="office_phone" placeholder="Office Phone">
                        </div>
                        </div>
			<div class="form-group">
                        <label for="mobile_phone" class="col-md-3 control-label">Mobile Phone</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="mobile_phone" placeholder="Mobile Phone">
                        </div>
                        </div>
			<div class="form-group">
                        <label for="address_line1" class="col-md-3 control-label">Address Line1</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="address_line1" placeholder="Address_Line1">
                        </div>
                        </div>
			<div class="form-group">
                        <label for="address_line2" class="col-md-3 control-label">Address Line2</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="address_line2" placeholder="Address_Line2">
                        </div>
                        </div>
			<div class="form-group">
                        <label for="city" class="col-md-3 control-label">City</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="city" placeholder="City">
                        </div>
                        </div>
			<div class="form-group">
                        <label for="state" class="col-md-3 control-label">State</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="state" placeholder="State">
                        </div>
                        </div>
			<div class="form-group">
                        <label for="zip" class="col-md-3 control-label">Zip</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="zip" placeholder="Zip">
                        </div>
                        </div>
			<div class="form-group">
                        <label for="country" class="col-md-3 control-label">Country</label>
                        <div class="col-md-9">
                           <input type="text" class="form-control" name="country" placeholder="Country">
                        </div>
                        </div>
			<div class="form-group">
                        <label class="col-md-3 control-label">Image</label>
                        <div class="col-md-9">
                           <input type="file" accept="image/*" required name="filepath">
                        </div>
                        </div>

                        <div class="form-group">
                        <!-- Button -->                                        
                        <div class="col-md-offset-3 col-md-9">
                           <button id="btn-signup" type="submit" class="btn btn-info btn-sm"><i class="icon-hand-right"></i> &nbsp Sign Up</button> 
                        </div>
                     </div>
                   </form>
               </div>
            </div>
         </div>
      </div>
	<!-- Signup Box -->

	<!-- Footer -->
	<div class="footer">
		<a href="#">FibroForword</a>
		<a href="#">Privacy &amp; Terms</a>
		<a href="#">Help</a>
	</div>
</div>

</body>
</html>

<script>

$(document).ready(function(){
$("#confirm_password").keyup(function(){
var password = $("#password").val();
if (  password != $("#confirm_password").val()) {
	$("#pass_error").html("<br> <center> Password doesn't match</center>");
} else
	$("#pass_error").html('');


});
});

</script>
