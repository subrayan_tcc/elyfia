     <div class="modal modal-info fade" id="myModal'.$details['id'].'" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
		<h2 class="modal-title">'.$details['firstname'].' '.$details['lastname'].'</h2>
              </div>
              <div class="modal-body">
                   <div class="row">
                       <div class="col-md-6 col-sm-6">
		          <h4 > Name: '.$details['firstname'].' '.$details['lastname'].'</h4>
			  <h4> Email: '.$details['email'].' </h4>
			  <h4> Home Phone: '.$details['home_phone'].' </h4>
			  <h4> Office Phone: '.$details['office_phone'].' </h4>
			  <h4> Mobile Phone: '.$details['mobile_phone'].' </h4>
		          <h2> Address </h2>
	 		  <h4> Line: '.$details['address_line1'].', '.$details['address_line2'].' </h4>
			  <h4> City: '.$details['city'].' </h4>
			  <h4> State: '.$details['state'].' </h4>
			  <h4> Zip: '.$details['zip'].' </h4>
			  <h4> Country: '.$details['country'].' </h4>
                     </div>
                       <div class="col-md-6 col-sm-6">
			<img src="user_image/'.$details['filepath'].'" class="img-rounded pull-right" alt="'.$details['firstname'].' " width="150" height="150" />
                     </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>

              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
