<?php
   $dashboard="active";
   require_once('config/config.php');    
   require_once('include/gen_functions.php');
   login();
   require_once('include/header.php');
   require_once('include/header_menu.php'); 
   $nurse_id = $_SESSION['nurse_id'];
   $active_users = 0;
   $inactive_users = 0;
   
   $active_devies = 0;
   $inactive_devices = 0;
   
   $sql = "SELECT status, count(*) as cnt from patients group by status";
   $result = mysqli_query($conn,$sql) or die("SQL Calorie Selection error".mysqli_error($conn));
   $numofrows=mysqli_num_rows($result);
   
   while($row=mysqli_fetch_array($result)){
      if ($row['status'] == 'Active') { $active_users = $row['cnt'];}
      if ($row['status'] == 'InActive') { $inactive_users = $row['cnt'];}
   }
   
   $sql = "SELECT id from restriced_admin_users";
   $result = mysqli_query($conn,$sql) or die("SQL Calorie Selection error".mysqli_error($conn));
   $admin_count_ =mysqli_num_rows($result);
   
   $sql = "SELECT id from patients";
   $result = mysqli_query($conn,$sql) or die("SQL Calorie Selection error".mysqli_error($conn));
   $client_count_=mysqli_num_rows($result);
   
   $sql = "SELECT id from nurse";
   $result = mysqli_query($conn,$sql) or die("SQL Calorie Selection error".mysqli_error($conn));
   $nurse_count_ =mysqli_num_rows($result);
   $nurse_count_=mysqli_num_rows($result);
   
   
   $sql = "SELECT * from patient_devices group by id";
   $result = mysqli_query($conn,$sql) or die("SQL Calorie Selection error".mysqli_error($conn));
   $numofrows=mysqli_num_rows($result);
   $device_count_=mysqli_num_rows($result);
   
   $sql = "SELECT conn_status, count(*) as cnt from patient_devices group by conn_status";
   $result = mysqli_query($conn,$sql) or die("SQL Calorie Selection error".mysqli_error($conn));
   $numofrows=mysqli_num_rows($result);
   
   while($row=mysqli_fetch_array($result)){
      if ($row['conn_status'] == '1') { $active_devices = $row['cnt'];}
      if ($row['conn_status'] == '0') { $inactive_devices = $row['cnt'];}
   }
   
   $latest_admins = get_latest_data('restriced_admin_users',$conn);
   $latest_nurse  = get_latest_data('nurse',$conn);
   
   
   
   function get_latest_data($table,$conn){
     $sql = "select firstname,lastname,filepath,created from $table order by id desc LIMIT 0, 12";
     $query = mysqli_query($conn,$sql) or die('error'.mysqli_error($conn));
     $arr;
     while($row = mysqli_fetch_array($query)){
      $arr[] = $row;
     }
     return $arr;
   }
   
   function __form_latest_data($data){ 
    $display = '<ul class="users-list clearfix">';  
    if(is_array($data)){
       
       foreach($data as $key => $val){
         $filepath =$val['filepath'];
           $display.='
                       <li>
                         <img src="user_image/'.$filepath.'" alt="User Image" height=40>
                         <a class="users-list-name" href="#">'.$val["firstname"].' '.$val["lastname"].'</a>
                         <span class="users-list-date">Today</span>
                       </li>';
       }
     }
     $display.= '</ul>';
      return $display;
   }
   
   $latest_admin_display = __form_latest_data($latest_admins);
   $latest_nurse_display = __form_latest_data($latest_nurse);

   $prev_date = date('Y-m-d', strtotime($date .' -1 day'));
   $nurse_id = $_SESSION['nurse_id'];
   $sql = "select gethealthid from patients where nurse_id = $nurse_id ";
   $result = mysqli_query($conn,$sql) or die("SQL Calorie Selection error".mysqli_error($conn));
   $arr;	
   while($row = mysqli_fetch_array($result)){

	$arr[] = $row['gethealthid'];
   }

   $ids = implode(',',$arr);
   if(count($arr) > 0){
	$sql = "select c.calories,st.steps_cnt,st.source,st.timestamp,sl.sleeping_cnt,CONCAT(p.firstname, ' ', p.lastname) as 
	name, p.expected_cal,p.expected_sleep,p.expected_steps from   calorie_info c 
	left join steps_info st    on   c.gethealthid = st.gethealthid 
	left join patients p       on   c.gethealthid = p.gethealthid 
	left join sleeping_info sl on   c.gethealthid = sl.gethealthid 
	where c.timestamp = '$prev_date' and st.timestamp = '$prev_date'  order by c.timestamp ";

	/* $sql = "select * from calorie_info c left join patients p on   c.gethealthid = p.gethealthid 
        where c.timestamp = '$prev_date' ";*/



	$result = mysqli_query($conn,$sql) or die("SQL Calorie Selection error".mysqli_error($conn));
	$display_needed = '';
	$i = 0;
	while($row = mysqli_fetch_array($result)){

	$expecetd_steps = $row['expected_steps'];
	$expecetd_calri = $row['expected_cal'];
	$expecetd_sleep = $row['expected_sleep'];

	$actaul_steps = $row['steps_cnt'];
	$actaul_calri = $row['calories'];
	$actaul_sleep = $row['sleeping_cnt'];

	$diff_steps = $row['expected_steps']*1 - $row['steps_cnt'] * 1;
	$diff_calri = 1*$row['expected_cal']   - 1*$row['calories'];
	$diff_sleep = 1*$row['expected_sleep'] - 1*$row['sleeping_cnt'];
	$color_steps = '#fff';
	$color_sleep = '#fff';
	$color_calor = '#fff';
	if($diff_steps < 0) {
		$color_steps = 'red';
	}
	if($diff_sleep < 0) {
		$color_sleep = 'red';
	}
	if($diff_calri < 0) {
		$color_calor = 'red';
	}

        $i = $i+1;
	if($diff_steps < 0 || $diff_calri < 0 || $diff_sleep < 0 ){
		$display_needed .= '
		<tr> 
		   <td>'.$i.'</td>
		   <td>'.$row["name"].' </td>	
		   <td>'.$row["source"].'</td>
		   <td>'.$row["calories"].'</td>
		   <td>'.$row["steps_cnt"]* 1 .' </td>
		   <td>'.$row["sleeping_cnt"]* 1 .' </td>
		   <td style="background:'.$color_steps.'">'.$diff_steps.' </td>
		   <td style="background:'.$color_calor.'">'.$diff_calri.' </td>
		   <td style="background:'.$color_sleep.'">'.$diff_sleep.' </td>
		   <td>'.$row["timestamp"].' </td>
		</tr>'; 
	    }	
	}
   }


   $table_content = '<table class="table no-margin">
		          <thead>
		          <tr>
		            <th>ID</th>
		            <th>Client</th>
		            <th>Device</th>
		            <th>Calorie</th>
		            <th>Steps</th>
		            <th>Sleep</th>

		            <th>Diff Calorie</th>
		            <th>Diff Steps</th>
		            <th>Diff Sleep</th>
		            <th>Date</th>
		          </tr>
		          </thead>
		          <tbody>
		          <!--<tr>
		            <td><a href="#">OR9842</a></td>
		            <td>Call</td>
		            <td><span class="label label-success">100</span></td>
		            <td><span class="label label-success">10</span></td>
		            <td><span class="label label-success">12</span></td>
		            <td><div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div></td>
		          </tr>-->
			 '.$display_needed.'
		          </tbody>
		        </table>';

   ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Dashboard
         <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <!-- fix for small devices only -->
         <div class="clearfix visible-sm-block"></div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="list_patients.php">
               <div class="info-box">
                  <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
                  <div class="info-box-content">
                     <span class="info-box-text">Clients</span>
                     <span class="info-box-number"><?= $client_count_ ?></span>
                  </div>
                  <!-- /.info-box-content -->
               </div>
               <!-- /.info-box -->
            </a>
         </div>
         <!-- /.col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="list_devices.php">
               <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa  fa-mobile-phone"></i></span>
                  <div class="info-box-content">
                     <span class="info-box-text">Devices</span>
                     <span class="info-box-number"><?= $device_count_ ?></span>
                  </div>
                  <!-- /.info-box-content -->
               </div>
               <!-- /.info-box -->
            </a>
         </div>
         <!-- /.col -->
	         <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="#">
               <div class="info-box">
                  <span class="info-box-icon bg-red"><i class="fa fa-stethoscope"></i></span>
                  <div class="info-box-content">
                     <span class="info-box-text">Default</span>
                     <span class="info-box-number"><!--<?= $nurse_count_ ?>--></span>
                  </div>
                  <!-- /.info-box-content -->
               </div>
               <!-- /.info-box -->
            </a>
         </div>
         <!-- /.col -->

      </div>
      <!-- /.row -->


<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Clients below limit</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
               
		<?php echo $table_content; ?>			

              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <!--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-right">View All Orders</a> -->
            </div>
            <!-- /.box-footer -->
          </div>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
   <!-- Create the tabs -->
   <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
   </ul>
   <!-- Tab panes -->
   <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
         <h3 class="control-sidebar-heading">Recent Activity</h3>
         <ul class="control-sidebar-menu">
            <li>
               <a href="javascript:void(0)">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                     <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                     <p>Will be 23 on April 24th</p>
                  </div>
               </a>
            </li>
            <li>
               <a href="javascript:void(0)">
                  <i class="menu-icon fa fa-user bg-yellow"></i>
                  <div class="menu-info">
                     <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                     <p>New phone +1(800)555-1234</p>
                  </div>
               </a>
            </li>
            <li>
               <a href="javascript:void(0)">
                  <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                  <div class="menu-info">
                     <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                     <p>nora@example.com</p>
                  </div>
               </a>
            </li>
            <li>
               <a href="javascript:void(0)">
                  <i class="menu-icon fa fa-file-code-o bg-green"></i>
                  <div class="menu-info">
                     <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                     <p>Execution time 5 seconds</p>
                  </div>
               </a>
            </li>
         </ul>

         <!-- /.control-sidebar-menu -->
         <h3 class="control-sidebar-heading">Tasks Progress</h3>
         <ul class="control-sidebar-menu">
            <li>
               <a href="javascript:void(0)">
                  <h4 class="control-sidebar-subheading">
                     Custom Template Design
                     <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                     <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>
               </a>
            </li>
            <li>
               <a href="javascript:void(0)">
                  <h4 class="control-sidebar-subheading">
                     Update Resume
                     <span class="label label-success pull-right">95%</span>
                  </h4>
                  <div class="progress progress-xxs">
                     <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                  </div>
               </a>
            </li>
            <li>
               <a href="javascript:void(0)">
                  <h4 class="control-sidebar-subheading">
                     Laravel Integration
                     <span class="label label-warning pull-right">50%</span>
                  </h4>
                  <div class="progress progress-xxs">
                     <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                  </div>
               </a>
            </li>
            <li>
               <a href="javascript:void(0)">
                  <h4 class="control-sidebar-subheading">
                     Back End Framework
                     <span class="label label-primary pull-right">68%</span>
                  </h4>
                  <div class="progress progress-xxs">
                     <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                  </div>
               </a>
            </li>
         </ul>
      </div>

      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
         <form method="post">
            <h3 class="control-sidebar-heading">General Settings</h3>
            <div class="form-group">
               <label class="control-sidebar-subheading">
               Report panel usage
               <input type="checkbox" class="pull-right" checked>
               </label>
               <p>
                  Some information about this general settings option
               </p>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
               <label class="control-sidebar-subheading">
               Allow mail redirect
               <input type="checkbox" class="pull-right" checked>
               </label>
               <p>
                  Other sets of options are available
               </p>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
               <label class="control-sidebar-subheading">
               Expose author name in posts
               <input type="checkbox" class="pull-right" checked>
               </label>
               <p>
                  Allow the user to show his name in blog posts
               </p>
            </div>
            <!-- /.form-group -->
            <h3 class="control-sidebar-heading">Chat Settings</h3>
            <div class="form-group">
               <label class="control-sidebar-subheading">
               Show me as online
               <input type="checkbox" class="pull-right" checked>
               </label>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
               <label class="control-sidebar-subheading">
               Turn off notifications
               <input type="checkbox" class="pull-right">
               </label>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
               <label class="control-sidebar-subheading">
               Delete chat history
               <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
               </label>
            </div>
            <!-- /.form-group -->
         </form>
      </div>
      <!-- /.tab-pane -->
   </div>


</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<footer class="main-footer">
   <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
   </div>
   <strong>Copyright &copy; 2017 <a href="#">FibroForward</a>.</strong> All rights reserved.
</footer>




