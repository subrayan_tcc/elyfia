<?php

/*function craete_menu($title,$url,$class,$access,$items,$active_flag){
  $array = array('title' =>$title,'url' =>$url,'class'=>$class,'access'=>$access,'active_flag'=>$active_flag,'items'=>$items);
  return $array; 
}


$items =  array('items' => array(
			array( 'title'=>'List Admin',				'url'=>'list_admin.php',				'class'=>'' ),
			array( 'title'=>'Create Admin',		'url'=>'create_admin.php',		'class'=>'' )
		));

$items);
print_r(craete_menu('Dashboard','dashboard.php','fa-dashboard',array(1),$items,'dashboard'));*/
		/* @uses the old ${section} variable to flag a menu as active (ex: $dashboard = 'active') */


$sitemenu = array(
	array(
		'title' => 'Dashboard',
		'url' => 'dashboard.php',
		'class' => 'fa-dashboard',
		'access' => array(1,2),
		'active_flag' => 'dashboard'
	),


	array(
		'title' => 'Dashboard',
		'url' => 'dashboard_nurse.php',
		'class' => 'fa-dashboard',
		'access' => array(3),
		'active_flag' => 'dashboard'
	),

array(
		'title' => 'Admin',
		'url' => '#',
		'class' => 'fa-th',
		'access' => array(1),
		'items' => array(
			array( 'title'=>'Create Admin','url'=>'create_admin.php','class'=>'' ),
			array( 'title'=>'List Admin','url'=>'list_admin.php','class'=>'' ),
			array( 'title'=>'Assign clients to nurse','url'=>'assign_client.php','class'=>'' ),
		),
		'active_flag' => 'admin',
	),

	array(
		'title' => 'Nurse',
		'url' => '#',
		'class' => 'fa-th',
		'access' => array(1,2),
		'items' => array(
			array( 'title'=>'Create Nurse','url'=>'create_nurse.php','class'=>'' ),
			array(  'title'=>'List Nurse', 'url'=>'list_nurse.php','class'=>'' ),

		),
		'active_flag' => 'nurse',
	),

	array(
		'title' => 'Patients',
		'url' => '#',
		'class' => 'fa-th',
		'access' => array(1,2,3),
		'items' => array(
			array( 'title'=>'Create Patients','url'=>'create_patient_ui.php','class'=>'' ),
			array( 'title'=>'List Patients','url'=>'list_patients.php','class'=>'' ),

		),
		'active_flag' => 'patients',
	),
	array(
		'title' => 'Devices Details',
		'url' => '#',
		'class' => 'fa-th',
		'access' => array(1,2),
		'items' => array(
			array( 'title'=>'List Devices','url'=>'list_devices.php','class'=>'' )
		),
		'active_flag' => 'devices',
	),
	array(
		'title' => 'Activites Details',
		'url' => '#',
		'class' => 'fa-files-o',
		'access' => array(1,2,3),
		'items' => array(
			array( 'title'=>'Calories','url'=>'calories_data.php',	'class'=>'' ),
			array( 'title'=>'Steps','url'=>'steps_data.php',	'class'=>'' ),
			array( 'title'=>'Sleeping','url'=>'sleeping_data.php',	'class'=>'' )
		),
		'active_flag' => 'activities',
	),
array(
		'title' => 'Payment History',
		'url' => 'payment.php',
		'class' => 'fa-th',
		'access' => array(1),
		'active_flag' => 'device',
	),

array(
		'title' => 'Settings',
		'url' => 'settings.php',
		'class' => 'fa-th',
		'access' => array(1,2),
		'active_flag' => 'device',
	),

array(
		'title' => 'Log Out',
		'url' => 'logout.php',
		'class' => 'fa-th',
		'access' => array(1,2,3),
		'active_flag' => 'device',
	),


);

?>



<!--$sitemenu = array(
	array(
		'title' => 'Dashboard',
		'url' => 'dashboard.php',
		'class' => 'fa-dashboard',
		'access' => array(1),
		/* @uses the old ${section} variable to flag a menu as active (ex: $dashboard = 'active') */
		'active_flag' => 'dashboard'
	),
	array(
		'title' => 'Patients',
		'url' => '#',
		'class' => 'fa-th',
		'access' => array(1),
		'items' => array(
			array( 'title'=>'List Patients',				'url'=>'list_patients.php',				'class'=>'' ),
			array( 'title'=>'Create Patients',		'url'=>'create_patient.php',		'class'=>'' )
		),
		'active_flag' => 'patients',
	),
	array(
		'title' => 'Devices',
		'url' => '#',
		'class' => 'fa-th',
		'access' => array(1),
		'items' => array(
			array( 'title'=>'List Devices',				'url'=>'list_devices.php',				'class'=>'' )
		),
		'active_flag' => 'devices',
	),
	array(
		'title' => 'Activites',
		'url' => '#',
		'class' => 'fa-files-o',
		'access' => array(1),
		'items' => array(
			array( 'title'=>'Calories',		'url'=>'calories_data.php',	'class'=>'' ),
			array( 'title'=>'Steps',		'url'=>'steps_data.php',	'class'=>'' ),
			array( 'title'=>'Sleeping',		'url'=>'sleeping_data.php',	'class'=>'' )
		),
		'active_flag' => 'activities',
	)
);-->
