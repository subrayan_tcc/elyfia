<?php 
$nurse="active";
require_once('config/config.php');
require_once('include/gen_functions.php');
login();
require_once('include/header.php');
require_once('include/header_menu.php');

if(isset($_REQUEST['delete'])){

  $del_id = $_REQUEST['delete'];
  $sql = "select admin_reference from nurse where id=$del_id";
  $result = mysqli_query($conn,$sql) or die('database error'.mysqli_error($conn));
  $row = mysqli_fetch_array($result);
  $admin_id = $row['admin_reference'];


  $sql = "delete from nurse where id = '$del_id' ";
  $query = mysqli_query($conn,$sql) or die('database error'.mysqli_error($conn));
    if($query){
         $sql = "delete from admin_users where id = '$admin_id' ";
         $query_t = mysqli_query($conn,$sql) or die('database error');
     }
   if($query && $query_t)
    $_SESSION['status'] = 'Nurse Deleted Successfully';
   else 
    $_SESSION['status'] = 'Failed Try Again';
}



	$sql = "select * from nurse";
	$result = mysqli_query($conn,$sql) or die("SQL Patients Selection error".mysqli_error($conn));
	$device_data=array();
	$numofrows=mysqli_num_rows($result);
	while($row=mysqli_fetch_array($result)){
	   $admin_data[]=$row;
	}

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List Nurse
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Nurse</a></li>
        <li class="active">List Nurse</li>
      </ol>

 <?php
if( isset($_SESSION['status'])){
    echo '<center><div style="width:50%;" class="callout callout-info">'.$_SESSION['status'].'</div></center>';
    unset($_SESSION['status']);
}


?>
    </section>

    <!-- Main content -->
    <section class="content">
<div class="row">

<div class="col-md-12">



<div class="box box-primary">
<div class="box-body">
  <table class="table list_table1 table-striped table-bordered table2excel " cellspacing="0" width="100%" id="table_payment_list" >
    <thead>
      <tr>
        <th width="20">DB id</th>
        <th>name</th>
        <th>Email</th>
      	<th>Password</th>
        <th>Contact</th>
        <th>Action</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php
         $modal_arr;
         foreach($admin_data as $details) {
           echo '<tr> 
             <td>'.$details['id'].'</td> 
             <td>'.$details['firstname'].'</td>
             <td>'.$details['email'].'</td>
             <td>'.$details['password'].'</td>
             <td>'.$details['mobile_phone'].'</td>
             <td><a class="btn btn-info btn-sm" style="text-decoration:none;" href="edit_nurse.php?edit_id='.$details['id'].'">Edit </a>  <a class="btn btn-info btn-sm delete_admin" style="text-decoration:none;" href="#" url="list_nurse.php?delete='.$details['id'].'">Delete</a></td>
             <td><a class="btn btn-info btn-sm" style="text-decoration:none;"  href="#"  data-toggle="modal" data-target="#myModal'.$details['id'].'">View Profile</a></td>
           </tr>';
	/*echo '
	<!-------------------------------- 
	Modal Content
	---------------------------------- -->
	<!-- Modal -->
	<div id="myModal'.$details['id'].'" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h2 class="modal-title">'.$details['firstname'].' '.$details['lastname'].'</h2>
	      </div>
	      <div class="modal-body text-center">
 
                  <img src="user_image/'.$details['filepath'].'" class="img-rounded" alt="'.$details['firstname'].' " width="304" height="236" />
		  <h4> First Name: '.$details['firstname'].' </h4>
		  <h4> Last Name: '.$details['lastname'].' </h4>
		  <h4> Email: '.$details['email'].' </h4>
		  <h4> Home Phone: '.$details['home_phone'].' </h4>
		  <h4> Office Phone: '.$details['office_phone'].' </h4>
		  <h4> Mobile Phone: '.$details['mobile_phone'].' </h4>
                  <h2> Address </h2>
 		  <h4> Line: '.$details['address_line1'].', '.$details['address_line2'].' </h4>
		  <h4> City: '.$details['city'].' </h4>
		  <h4> State: '.$details['state'].' </h4>
		  <h4> Zip: '.$details['zip'].' </h4>
		  <h4> Country: '.$details['country'].' </h4>

	      </div>
	      <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>'; */

echo '     <div class="modal modal-info fade" id="myModal'.$details['id'].'" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
		<h2 class="modal-title">'.$details['firstname'].' '.$details['lastname'].'</h2>
              </div>
              <div class="modal-body">
                   <div class="row">
                       <div class="col-md-6 col-sm-6">
		          <h4 > Name: '.$details['firstname'].' '.$details['lastname'].'</h4>
			  <h4> Email: '.$details['email'].' </h4>
			  <h4> Home Phone: '.$details['home_phone'].' </h4>
			  <h4> Office Phone: '.$details['office_phone'].' </h4>
			  <h4> Mobile Phone: '.$details['mobile_phone'].' </h4>
		          <h2> Address </h2>
	 		  <h4> Line: '.$details['address_line1'].', '.$details['address_line2'].' </h4>
			  <h4> City: '.$details['city'].' </h4>
			  <h4> State: '.$details['state'].' </h4>
			  <h4> Zip: '.$details['zip'].' </h4>
			  <h4> Country: '.$details['country'].' </h4>
                     </div>
                       <div class="col-md-6 col-sm-6">
			<img src="user_image/'.$details['filepath'].'" class="img-rounded pull-right" alt="'.$details['firstname'].' " width="150" height="150" />
                     </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>

              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->';


            
          }    
      ?>
     </tbody>
   </table>
  </div>
</div>

<script>
$(document).ready(function(){
 $('.table').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
 $(".delete_admin").click(function(){
  var admin = $(this).attr('url');
  var decide = confirm("Are you sure to delete this?");
  if(decide){
    window.location.href = admin;
  }
 });

});
</script>
