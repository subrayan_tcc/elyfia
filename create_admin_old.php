<?php
$admin="active";
require_once('config/config.php');    
require_once('include/gen_functions.php');
login();
require_once('include/header.php');
require_once('include/header_menu.php'); 

if(isset($_SESSION['insert'])){
   echo "<div class='text-center alert alert-success'><strong>Success! </strong> ". $_SESSION['insert']." </div>";
   unset($_SESSION['insert']);
}

?>

<div class="content-wrapper" style="background:#fff!important;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Admin
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Admin</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
   <form method="post" action="insert_data.php" enctype="multipart/form-data"> 
  <div class="box box-danger">
    <div class="col-md-6">
            <div class="box-body">
              <div class="form-group">
                <label>First Name</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                  <input type="text" class="form-control" required placeholder="First Name" name="firstname" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
            
              <div class="form-group">
                <label>Email</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                  </div>
                  <input type="email" class="form-control" required placeholder="email" name="email" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

      

              <!-- phone mask -->
              <div class="form-group">
                <label>Home Phone:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" name="home_phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

   <!-- phone mask -->
              <div class="form-group">
                <label>Office Phone:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" name="office_phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->


              <!-- phone mask -->
              <div class="form-group">
                <label>Mobile Phone:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-mobile"></i>
                  </div>
                  <input type="text" class="form-control" name="mobile_phone"
                         data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

        <!-- Date mm/dd/yyyy -->
              <div class="form-group">
                <label>Date Of Birth</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="date_of_birth" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

        <!-- Date mm/dd/yyyy -->
              <div class="form-group">
                <label>Date Of join</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="date_of_join" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <div class="form-group">
          	<button type="submit" class="text-center pull-right btn">Submit</button>
 	      </div>	
         </div>
         </div>
    <div class="col-md-6">
       <div class="box-body">
              <div class="form-group">
                <label>Last Name</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                  <input type="text" name="lastname" class="form-control" required placeholder="Last Name" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
    <!-- phone mask -->
              <div class="form-group">
                <label>Password</label>
                <label></label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="password" name="password" class="form-control" placeholder="Password" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

    <!-- phone mask -->
              <div class="form-group">
                <label>Address</label>
                <label></label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" name="address_line1" class="form-control" placeholder="Line 1" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
    <!-- phone mask -->
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="address_line2"
                          placeholder="Line 2" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

<!-- phone mask -->
              <div class="form-group">


                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="city"
                         placeholder="City" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
<!-- phone mask -->
              <div class="form-group">


                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="state"
                         placeholder="State" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
              <div class="form-group">


                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="zip"
                          placeholder="Zip/Postal" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
              <div class="form-group">


                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="country"
                         placeholder="Country" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <div class="form-group">
                <label>User Image</label>
                 <input type="file" accept="image/*" required name="filepath"  />
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

    </div>

             <!-- <div class="form-group">
                <div class="input-group">
                  
                  <input name="admin" type="radio" class=""> Admin
                  <input name="admin" type="radio" class=""> Super Admin                        
                </div>
                <!-- /.input group -->
              <!--</div>
              <!-- /.form group -->

    </form>
  </section>

</div>
