<?php 
$patients="active";
require_once('config/config.php');
require_once('include/gen_functions.php');
login();
require_once('include/header.php');
require_once('include/header_menu.php');


    if ($_REQUEST['action'] == 'delete') {
        $action='delete';
        $page_title = "Delete Patient";
        $field_all_disabled =' disabled ';



    }


$sql = "select * from patients order by id desc ";

$display_header='
    <thead>
      <tr>
        <th>Action</th>
        <th>DB id</th>
        <th>Patient Name</th>
        <th>Patient Email</th>
        <th>Patient ID</th>
        <th>Access Token</th>
        <th>Get Health Id</th>
      	<th>Status</th>
        <th>Created Time</th>
      </tr>
    </thead>';

if(isset($_SESSION['nurse_id'])){
  $nid = $_SESSION['nurse_id'];
  $sql = "select * from patients where nurse_id = $nid  order by id desc" ;
$display_header='
    <thead>
      <tr>
        <th>Action</th>
        <th>DB id</th>
        <th>Patient Name</th>
        <th>Patient Email</th>
        <th>Patient ID</th>
      	<th>Status</th>
        <th>Created Time</th>
	<th>Add Values</th>
      </tr>
    </thead>';

}

$result = mysqli_query($conn,$sql) or die("SQL Patients Selection error".mysqli_error($conn));
$patient_data=array();
$numofrows=mysqli_num_rows($result);
while($row=mysqli_fetch_array($result)){
   $patient_data[]=$row;
}
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List Patients
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Patients</a></li>
        <li class="active">List Patients</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<div class="row">

<div class="col-md-12">
 <?php echo $sql;
if( isset($_SESSION['status'])){
    echo '<center><div style="width:50%;" class="callout callout-info">'.$_SESSION['status'].'</div></center>';
    unset($_SESSION['status']);
}


?>

<link rel="stylesheet" href="css/style_new.css">


<div class="box box-primary">
            <div class="box-header with-border"> <!-- <a href="create_patient.php"><button class="btn btn-success" >Create New Patient</button></a>-->
              <h3 class="box-title">List Patients</h3>
            </div>
<div class="box-body">
  <table class="table list_table1 table-striped table-bordered table2excel " cellspacing="0" width="100%" id="table_payment_list" >
<?= $display_header; ?>
    <tbody>
      <?php
         foreach($patient_data as $details) {
	  $get_id_ = $details['gethealthid'];
	  $acc_tok_ = $details['access_token'];
	  $db_id = $details['id'];
           echo '<tr> 
             <td style="width:18%";>

		<a style="text-decoration:none;" href="create_patient.php?action=edit&id='.$details['id'].'">
			<div class="tooltip1">
				<span class="glyphicon glyphicon-edit"  id = '.$details['id'].' ></span>
				<div class="tooltiptext1">Edit</div>                 
			</div>  
                </a>	
		<a title="text" style="text-decoration:none;" class="__del_user" ref="list_patients.php?action=delete&id='.$details['id'].'"   attr_acc_tok = '.$acc_tok_.'  attr_get_id = '.$get_id_.' db_id='.$db_id.'>
			<div class="tooltip2">
				<span class="glyphicon glyphicon-trash" id = '.$details['id'].' ></span> 
				<div class="tooltiptext2">Delete</div>                 
			</div>  
                </a>
		<a style="text-decoration:none;" href="list_devices.php?access_token='.$details['access_token'].'"> 
			<div class="tooltip3">
				<span class="glyphicon glyphicon-phone" id = '.$details['access_token'].' ></span> 
				<div class="tooltiptext3">View</div>                 
			</div>  
                </a>
                <a style="text-decoration:none;" href="calories_data.php?gethealthid='.$details['gethealthid'].'"> 
			<div class="tooltip4">
				<span class="glyphicon glyphicon-heart " id = '.$details['gethealthid'].' ></span>
				<div class="tooltiptext4">View</div>
			</div>  
                </a>
                <a style="text-decoration:none;" href="steps_data.php?gethealthid='.$details['gethealthid'].'">
			<div class="tooltip5">
               		<img src="img/walk2.jpg" style="width:12px;" style="height:12px;" id = '.$details['gethealthid'].' > 
                  		<div class="tooltiptext5">View</div>
			</div>
                </a><br>
                <a style="text-decoration:none;" href="all_dtls_data.php?gethealthid='.$details['gethealthid'].'&access_token='.$details['access_token'].'"> 
                  <button class="btn btn-info btn-xs" type="view" id = '.$details['gethealthid'].' >All Details</button> 
                </a>


              </td>
             <td>'.$details['id'].'</td> 
             <td>'.$details['firstname'].' '.$details['lastname'].'</td>
             <td>'.$details['email'].'</td>
             <td>'.$details['uid'].'</td>';
 	if(!isset($_SESSION['nurse_id'])){     
 	     echo' <td>'.$details['access_token'].'</td>
             <td>'.$details['gethealthid'].'</td>';
	}
             echo '<td>'.$details['status'].'</td>
             <td>'.$details['created_date'].'</td>';
	if(isset($_SESSION['nurse_id'])){     
 	     echo' <td><button class="btn btn-primary _class_et" data-toggle="modal" data-target="#myModal" atr="'.$details['id'].'">Add</button></td>';
	 }

  echo ' </tr>';
          }    
      ?>
     </tbody>
   </table>
  </div>
</div>



<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" id="close_me" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Add client's minium activity limit</h4>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="__msteps" placeholder="Minimum Steps">  <br />
        <input type="text" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="__mcalor" placeholder="Minimum Calories to be burned"><br />
        <input type="text" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="__msleep" placeholder="Minimum Sleep"><br />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="__save_minimu_activities">Submit</button>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
$('.table').DataTable( {
        "order": [[ 0, "desc" ]]
    });

$(".__del_user").click(function(e){

  e.preventDefault();
  var gethealthid  = $(this).attr("attr_get_id");
  var access_token = $(this).attr("attr_acc_tok");
  var database_id  = $(this).attr("db_id");

       $.ajax({
		url:'delete_patient.php',
		method:'post',	
		data:{gethealthid:gethealthid,access_token_:access_token,database_id:database_id,type:'delete'},
		success:function(data){
			alert(data);
			window.location.reload();
		},
		error:function(){

		alert('Check the server side code');
		}
	});

});



$("#__save_minimu_activities").click(function() {
	var steps  = $("#__msteps").val();
	var calor  = $("#__mcalor").val();
	var sleep  = $("#__msleep").val();
	var client = localStorage.getItem("client_id");	

  if(steps != '' && calor != '' && sleep != '' && localStorage.getItem("client_id") != null ){
       $.ajax({
		url:'clients_limit_activities.php',
		method:'post',	
		data:{steps:steps,calor:calor,sleep:sleep,client:client,type:'putdata'},
		success:function(data){
			alert(data);
			$("#close_me").trigger('click');
		},
		error:function(){

		alert('Check the server side code');
		}
	});
   }
});


  $('._class_et').click(function(){
	localStorage.setItem("client_id",$(this).attr('atr'));
	var client = $(this).attr('atr');

       $.ajax({
		url:'clients_limit_activities.php',
		method:'post',	
		data:{client:$(this).attr('atr'),type:'getdata'},
		success:function(data){
			console.log(data);
		   	var obj = JSON.parse(data); console.log(obj.steps);
			$("#__msteps").val(obj.steps);
			$("#__mcalor").val(obj.calories);
			$("#__msleep").val(obj.sleep);

		},
		error:function(){

		alert('Check the server side code');
		}
	});
	
  }); 
});
</script>
