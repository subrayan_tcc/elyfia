<?php
require_once('config/config.php');
require_once('include/gen_functions.php');
require_once('include/header.php');

if(isset($_POST['login'])) {
  extract($_POST); 
  $user = sanit_data($user);
  $pass = md5(sanit_data($pass));
  $sql = "select * from admin_users  where email = '$user' and password = '$pass' ";
  $exec_sql = mysqli_query($conn,$sql) or die(mysqli_error($conn));
  if(mysqli_num_rows($exec_sql) == 1) {
    $row = mysqli_fetch_array($exec_sql) or die(mysqli_error($conn));
    if($user == $row['email'] && $pass == $row['password']) {
      $_SESSION['admin'] = $row['name']; 
      $_SESSION['admin_id']=$row['id']; 
      $_SESSION['admin_id_level']=$row['level']; 
      $_SESSION['profile_image']=$row['user_image']; 
      $_SESSION['main_access_token']=$row['main_access_token'];

      if ($row['level'] == 1) {
        header('location: dashboard.php');
        exit;
      }
      if ( $row['level'] == 2) {
        header('location: dashboard_nurse.php');
        exit;
      }

  if ( $row['level'] == 3) {
	$aid = $row['id'];
	$sql = "select n.id as nid from admin_users a left join nurse n on a.id = n.admin_reference where a.id = $aid ";
 	$exec_sql = mysqli_query($conn,$sql) or die(mysqli_error($conn));
	$row = mysqli_fetch_array($exec_sql);
	$_SESSION['nurse_id'] = $row['nid'];
        header('location: dashboard_nurse.php');
        exit;
      }
      header('location: dashboard.php');
      exit;
    } else {
      $error = '<p style="color:red"> Wrong User Name/Password</p>';
    }
  }  else {
    $error = '<p style="color:red"> Wrong User Name/Password</p>';
  }
}
  if(isset($_SESSION['error_status_check'])){

    $error = '<p style="color:red">Session Expired</p>';
    unset($_SESSION['error_status_check']);
  }

if(isset($_SESSION['insert'])){
   echo "<div class='text-center alert alert-success'><strong>User Created Successfully! </strong>  </div>";
   unset($_SESSION['insert']);
}

?>

      <link rel="stylesheet" type="text/css" href="css/login_style.css">


<body class="hold-transition login-page" id="body_container">

      <div class="container">
         <form accept-charset="utf-8" class="form-horizontal form-signin" method="POST" action="#">
            <div class="modal-header">
               <p>
                  <a href="#" target="_blank">
                     <img class="logo_img_2" src="http:/img/logo login.png" />         
                     <!--<h4>Account Login</h4>--->
                  </a>
               </p>
            </div>
            <div class="inner">
               <div class="row">
                  <div class="col">
                     <p style="font-size:17px;font-weight:700">login<img style="width:17px;margin-top:-5px" src="http://invoice.grandcoeinvoicing.com/assets/img/login-sec.png" /></p>
			<?php 
				if(isset($error)){
				echo '<center>'.$error.'</center>';
				}
			?>
                     <br>
                        Email
                        <input required="" class="form-control" placeholder="Email address" id="email" type="text" name="user"><br>
                        Password
                        <input required="" class="form-control" placeholder="Password" id="password" type="password" name="pass"><br>
			<p class="forget">Did you forget your password?<br><a class='btn-link' href="#">Recover it here</a></p>
                     
			<!--<div class="row">
        			<div class="col-xs-8">
          			<div class="checkbox icheck">
          			</div>
        			</div>
			</div>-->
                     <button type='submit' class='btn btn-primary' name='login' id='loginButton'>Login</button>
                  </div>
               </div>
            </div>
            <input class="form-control" type="hidden" name="_token" value="DjxWL0EwhKrJFpecuQop3RUHwgPUmTtqDLAjKT2I">
         </form>
    
<script>
$(document).ready(function(){
$("#inputEmail").focus();
});
</script>
</body>
</html>
