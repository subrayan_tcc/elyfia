<?php 
$admin="active";
require_once('config/config.php');
require_once('include/gen_functions.php');
login();
require_once('include/header.php');
require_once('include/header_menu.php');

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Assign Clients
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Assign Clients</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<div class="row">

<div class="col-md-12">
 <?php
if( isset($_SESSION['status'])){
    echo '<center><div style="width:50%;" class="callout callout-info">'.$_SESSION['status'].'</div></center>';
    unset($_SESSION['status']);
}


$sql = "select id,concat(firstname,' ',lastname) as name,gethealthid from patients where nurse_id = 0 order by id desc";
$result = mysqli_query($conn,$sql);
$arr;
while($row = mysqli_fetch_array($result)){
 $arr[] = array('id'=>$row['id'],'name'=>$row['name'],'gethealthid'=>$row['gethealthid']);

}


$sql = "select id,concat(firstname,' ',lastname) as name from nurse";
$result = mysqli_query($conn,$sql);
$n_arr;
while($row = mysqli_fetch_array($result)){
 $n_arr[] = array('id'=>$row['id'],'name'=>$row['name']);
}

	$sql = "select * from nurse";
	$result = mysqli_query($conn,$sql) or die("SQL Patients Selection error".mysqli_error($conn));
	$device_data=array();
	$numofrows=mysqli_num_rows($result);
	while($row=mysqli_fetch_array($result)){
	   $admin_data[]=$row;
	}

?>

  <link rel="stylesheet" type="text/css" href="css/multi-select.css">
  <script src="js/jquery.multi-select.js"></script>


</div>
<div class="box-body"  style="height:400px!important;">
<div class="row">
    <div class="col-md-2" "> 

        <select id="dates-field2" class="multiselect-ui form-control" style="height:100px!important;overflow-x: auto !important;" multiple="multiple">
<?php

	foreach($arr as $key => $val){
		$id = $val['id']; $name = $val['name'];  
		$name = ($name !=  " " )?$name:'Name not given';
		echo "<option value='$id'>$name</option>";
	
	}
?>
        </select>
    </div>






    <div class="col-md-3"> 

        <select id="n_list" class="form-control">
	<option value=none>Nurse List</option>
<?php
	foreach($n_arr as $key => $val){
		$id = $val['id']; $name = $val['name'];
		echo "<option value='$id'>$name</option>";
	
	}
?>
        </select>
    </div>


    <div class="col-md-6 text-center" > 	
	<p><button id="get_clients_" class="btn btn-primary">Assign</button></p>
    </div>

</div>






	<!--<div class="col-md-12 col-sm-12 col-lg-12">
	  <center> <button class="text-center get_clients_ btn btn-primary"> Assign</button> </center><br> <br>
	</div>
	<div class="col-md-6 col-sm-6 col-lg-6">
    	<h4> Clients </h4> <br>
	<table class="table table-striped" id="small_table___">
	<thead>
	 <tr>
		<th></th>
		<th></th> 
	 </tr>

	</thead>
<?php

	foreach($arr as $key => $val){
		$id = $val['id']; $name = $val['name']; $gethealthid = $val['gethealthid'];
		echo "<tr><td><input type='checkbox' class='fullest' value='$id'> </td> <td>$name($gethealthid) </td></tr>";
	
	}
?>

	</table>
	</div>


	<div class="col-md-6 col-sm-6 col-lg-6">

    	<h4> Nurse </h4> <br>
	<form id="myForm">
	<table class="table table-striped">
	<thead>
	 <tr>
		<th></th>
		<th></th> 
	 </tr>
	<tbody>

<?php

 	foreach($n_arr as $key => $val){
		$id = $val['id']; $name = $val['name'];
		echo "<tr><td><input type='radio' name='fullest' class='n_deatils' value='$id'></td><td> $name  </td></tr>";
	
	}
?>
	</tbody>
	</table>
</form>
	</div>-->


<script type="text/javascript">
$(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>


<script>
$(document).ready(function(){
  $('#small_table_').DataTable( {
        "lengthMenu": [[10, 50, 100, -1], [100, 200, 300, "All"]],
        "order": [[ 0, "desc" ]]
    });


 $("#get_clients_").click(function(){

 	var clients = $("#dates-field2").val();
	var nurse = $("#n_list").val();




	if(clients == null || nurse == 'none')
	 alert('please select clients');
	else{
	$.ajax({
		url:'clients_to_nurse.php',
		method:'post',
		data:{data:clients,nurse:nurse},
		success:function(data){
		  	alert(data);
			window.location.reload();
		},
		error:function(){
			alert('Request failed');
		}
 	  });
	}
 });



  $(".get_clients_").click(function(){
var sList = "";
var arr = [];

var nurse = $('input[name=fullest]:checked', '#myForm').val()

$('input[type=checkbox]').each(function () {
    var sThisVal = (this.checked ? "1" : "0");
    if(sThisVal == 1)
	arr.push($(this).val());
});



if(arr.length == 0 || typeof nurse == 'undefined')
 alert('please select clients');
else{
	$.ajax({
		url:'clients_to_nurse.php',
		method:'post',
		data:{data:arr,nurse:nurse},
		success:function(data){
		  alert(data);
			window.location.reload();
		},
		error:function(){
			alert('Request failed');
		}
	});
}

console.log(arr);


  });
});
</script>










