<?php
$patients="active";
require_once('config/config.php');    
require_once('include/gen_functions.php');
login();
require_once('include/header.php');
require_once('include/header_menu.php'); 

if(isset($_REQUEST['edit_id'])){

  $edit_id = $_REQUEST['edit_id'];
  $data = get_row($edit_id,$conn,'patients_details');
}

 function form_data($val,$data){ 
  if(isset($data[$val]))
    return $data[$val];
  else
   return $val;
 }

?>

<div class="content-wrapper" style="background:#fff!important;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Patient
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Patient</li>
      </ol>
 <?php
if( isset($_SESSION['insert'])){
    echo '<center><div style="width:50%;" class="callout callout-info">'.$_SESSION['insert'].'</div></center>';
    unset($_SESSION['insert']);
}


?>
    </section>
    <!-- Main content -->
    <section class="content">
   <form method="post" action="insert_data.php" enctype="multipart/form-data"> 
  <div class="box box-danger">
    <div class="col-md-6">
            <div class="box-body">
              <div class="form-group">
                <label>First Name</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                  <input type="text" class="form-control" required placeholder="First Name" value="<?= form_data('firstname',$data); ?>" name="firstname">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
            
              <div class="form-group">
                <label>Email</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                  </div>
                  <input type="email" class="form-control" required placeholder="email" value="<?= form_data('email',$data); ?>" name="email" >
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

      

              <!-- phone mask -->
              <div class="form-group">
                <label>Home Phone:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" name="home_phone" value="<?= form_data('home_phone',$data); ?>">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

   <!-- phone mask -->
              <div class="form-group">
                <label>Office Phone:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" name="office_phone" value="<?= form_data('office_phone',$data); ?>" >
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->


              <!-- phone mask -->
              <div class="form-group">
                <label>Mobile Phone:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-mobile"></i>
                  </div>
                  <input type="text" class="form-control" name="mobile_phone" value="<?= form_data('mobile_phone',$data); ?>">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

        <!-- Date mm/dd/yyyy -->
              <div class="form-group">
                <label>Date Of Birth</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="date_of_birth" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'" id="datepicker_dob" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

        <!-- Date mm/dd/yyyy -->
              <div class="form-group">
                <label>Date Of join</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="date_of_join" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'" id="datepicker_doj" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <div class="form-group">
          	<button type="submit" class="text-center pull-right btn btn-primary">Edit</button>
 	      </div>	
         </div>
         </div>
    <div class="col-md-6">
       <div class="box-body">
              <div class="form-group">
                <label>Last Name</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                  <input type="text" name="lastname" class="form-control" required placeholder="Last Name" value="<?= form_data('lastname',$data); ?>">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
    <!-- phone mask -->
              <div class="form-group">
                <label>Password</label>
                <label></label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="password" name="password" class="form-control" placeholder="Password" value="<?= form_data('password',$data); ?>">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

    <!-- phone mask -->
              <div class="form-group">
                <label>Address</label>
                <label></label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" name="address_line1" class="form-control" placeholder="Line 1" value="<?= form_data('address_line1',$data); ?>">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
    <!-- phone mask -->
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="address_line2"
                          placeholder="Line 2" value="<?= form_data('address_line2',$data); ?>">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

<!-- phone mask -->
              <div class="form-group">


                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="city"
                         placeholder="City" value="<?= form_data('city',$data); ?>">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
<!-- phone mask -->
              <div class="form-group">


                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="state" value="<?= form_data('state',$data); ?>"
                         placeholder="State" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
              <div class="form-group">


                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="zip" value="<?= form_data('zip',$data); ?>"
                          placeholder="Zip/Postal" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
              <div class="form-group">


                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-globe"></i>
                  </div>
                  <input type="text" class="form-control" name="country" value="<?= form_data('country',$data); ?>"
                         placeholder="Country" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <div class="form-group">
                 <img class="pull-right" src="user_image/<?= form_data('filepath',$data); ?>" width=50 height=50>
                 <input type="file" accept="image/*" required name="filepath"  />

                <!-- /.input group -->
              </div>
              <!-- /.form group -->

    </div>

             <!-- <div class="form-group">
                <div class="input-group">
                  
                  <input name="admin" type="radio" class=""> Admin
                  <input name="admin" type="radio" class=""> Super Admin                        
                </div>
                <!-- /.input group -->
              <!--</div>
              <!-- /.form group -->

    </form>
  </section>

</div>


