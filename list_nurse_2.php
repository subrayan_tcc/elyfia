<?php 
$nurse="active";
require_once('config/config.php');
require_once('include/gen_functions.php');
login();
require_once('include/header.php');
require_once('include/header_menu.php');


  $sql = "select * from nurse";

$result = mysqli_query($conn,$sql) or die("SQL Patients Selection error".mysqli_error($conn));
$device_data=array();
$numofrows=mysqli_num_rows($result);
while($row=mysqli_fetch_array($result)){
   $admin_data[]=$row;
}

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List Nurse
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Nurse</a></li>
        <li class="active">List Nurse</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<div class="row">

<div class="col-md-12">
 <?php
if( isset($_SESSION['status'])){
    echo '<center><div style="width:50%;" class="callout callout-info">'.$_SESSION['status'].'</div></center>';
    unset($_SESSION['status']);
}


?>


<div class="box box-primary">
<div class="box-body">
  <table class="table list_table1 table-striped table-bordered table2excel " cellspacing="0" width="100%" id="table_payment_list" >
    <thead>
      <tr>
        <th>DB id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Country</th>
      	<th>Password</th>
      </tr>
    </thead>
    <tbody>
      <?php
         foreach($admin_data as $details) {
           echo '<tr> 
             <td>'.$details['id'].'</td> 
             <td>'.$details['firstname'].'</td>
             <td>'.$details['email'].'</td>
             <td>'.$details['country'].'</td>
             <td>'.$details['password'].'</td>
           </tr>';
          }    
      ?>
     </tbody>
   </table>
  </div>
</div>

<script>
$(document).ready(function(){
$('.table').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
});
</script>
